function multiplicaMatriz(a, b) {
    var aNumLinhas = a.length, aNumColuna = a[0].length,
        bNumColuna = b[0].length,
        matriz = new Array(aNumLinhas);
    for (var i = 0; i < aNumLinhas; ++i) {
        matriz[i] = new Array(bNumColuna);
        for (var y = 0; y < bNumColuna; ++y) {
            matriz[i][y] = 0;
            for (var w = 0; w < aNumColuna; ++w) {
                matriz[i][y] += a[i][w] * b[w][y];
        }
      }
    }
    return matriz;
}

function mostrar(matriz) {
    for (var i = 0; i < matriz.length; ++i) {
      console.log(matriz[i].join(' '));
    }
}

//////////////////////////////////////////////////////////

/* Exemplo 1:
var A = [[2,-1], [2,0]];
var B = [[2,3], [-2,1]];
let resultado = multiplicaMatriz(A, B);

console.log("Matriz A:");
mostrar(A);
console.log("Matriz B:");
mostrar(B);
console.log("Multiplicação das Matrizes:");
console.log(mostrar(resultado)); */

//////////////////////////////////////////////////////////

/* Exemplo 2:
var A = [[4,0], [-1,-1]];
var B = [[-1,3], [2,7]];
let resultado = multiplicaMatriz(A, B);

console.log("Matriz A:");
mostrar(A);
console.log("Matriz B:");
mostrar(B);
console.log("Multiplicação das Matrizes:");
console.log(mostrar(resultado)); */

//////////////////////////////////////////////////////////