let nota1 = Math.floor(Math.random() * 11);
let nota2 = Math.floor(Math.random() * 11);
let nota3 = Math.floor(Math.random() * 11);
let media = Math.round((nota1 + nota2 + nota3) / 3)

console.log("Nota 1: " + nota1)
console.log("Nota 2: " + nota2)
console.log("Nota 3: " + nota3)
console.log("Sua média foi: " + media)

if (media >= 6) {
    console.log("Aprovado!")
} else {
    console.log("Reprovado!")
}